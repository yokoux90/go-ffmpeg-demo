[FFmpeg Download Link](http://ffmpeg.org/download.html)

### 1 Configurate Env Variable

```sh
# .bashrc/.zshrc
export FFMPEG_HOME=/your/ffmpeg/location
export PATH=$PATH:$FFMPEG_HOME/bin
```

### 2 Extract the first frame from video/gif/jpg

```sh
ffmpeg -i file_example_MP4_1920_18MG.mp4 -y -ss 0 -frames 1 -f image2 -s 600x600 test.jpg
```

### 3 Call in Go

```go
package main

import (
	"bytes"
	"log"
	"os"
	"os/exec"
	"path"
)

func main() {
	rootPath, _ := os.Getwd()

	// Input file path: ./assets/file_example_MP4_1920_18MG.mp4
	input := path.Join(rootPath, "assets", "file_example_MP4_1920_18MG.mp4")
	// Output file path: ./assets/file_example_MP4_1920_18MG.jpg
	output := path.Join(rootPath, "output", "file_example_MP4_1920_18MG.jpg")

	log.Println("[Input] ", input)

	// ffmpeg -i input_file -ss 0 -frames 1 -y -s 600x600 -f image2 output_file
	// -ss 提取第0秒的帧，即第一帧
	// -frame n 输出帧数，输出成图片所以设为1
	// -f image2  输出格式，固定为image2
	// -y 覆盖已有文件
	// -s 输出文件大小
	cmdArguments := []string{"-i", input, "-ss", "0",
		"-frames", "1", "-y", "-s", "600x600", "-f", "image2", output}

	cmd := exec.Command("ffmpeg", cmdArguments...)

	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("command output: %q \n", out.String())
	log.Println("[Output] ", output)
}
```
